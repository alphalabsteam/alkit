//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import UIKit
import ALKit
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  let moduleLocator: ALServiceLocator = ALServiceLocatorImp.shared
  
  let buildConfig: ALBuildConfig = {
    var buildConfig: ALBuildConfig = .release
    #if DEBUG
    buildConfig = .debug
    #endif
    return buildConfig
  }()

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    createSplashScreen()
        
    FirebaseApp.configure()
    
    moduleLocator.alConfigurator
      .configure(delegate: self, buildConfig: buildConfig, timeOutSec: 15, modulePrintEnabled: true)
      .configureAppsFlyer(delegate: self, appsFlyerDevKey: "12345", appleAppID: "123")
      .configureRemoteConfig(delegate: self)
      .configureAmplitude(withApiKey: "abc")
      .configureFlurry(withApiKey: "abcde")
        
    return true
  }
  
  private func createSplashScreen() {
    let vc = UIViewController()
    vc.view.backgroundColor = .lightGray
    window = UIWindow(frame: UIScreen.main.bounds)
    window!.makeKeyAndVisible()
    window!.rootViewController = vc
  }
  
  private func createAppInternalScreen() {
    let vc = UIViewController()
    vc.view.backgroundColor = .gray
    window!.rootViewController = vc
  }
  
}


// MARK: - ALAppsFlyerServiceDelegate
extension AppDelegate: ALAppsFlyerDelegate {
  
  func appsFlyer(_ alAppsFlyer: ALAppsFlyer, didReceiveConversionData installData: [AnyHashable : Any]?, error: Error?) {
    guard
      error == nil,
      alAppsFlyer.isFirstLaunch
      else { return }
    
    // log events
    // set user properties
    
//    let sourceForUserProperty = moduleLocator.alSettings.mediaSource ?? "Organic"
//    moduleLocator.amplitude.setUserProperty(.userSource, withValue: sourceForUserProperty, policy: .once)
//
//    let sourceForEventProperty = moduleLocator.alSettings.mediaSource != nil ? "Campaign" : "Organic"
//    moduleLocator.amplitude.logEvent(.sessionStartSource, withEventProperties: [.source : sourceForEventProperty], policy: .multiple)
  }
  
}

// MARK: - ALRemoteConfigDelegate
extension AppDelegate: ALRemoteConfigDelegate {
  
  func remoteConfig(_ alRemoteConfig: ALRemoteConfig, didConfigureWithStatus status: RemoteConfigFetchStatus, error: Error?) {
    if let type = alRemoteConfig.screenType(forSpot: "after_tutorial") {
      print(type)
    }
  }

}

// MARK: - ALConfiguratorDelegate
extension AppDelegate: ALConfiguratorDelegate {
  
  func didConfigureRequiredServices(byTimeout: Bool, secondsPassed: TimeInterval) {
    // we need to setup UI here
    // app coordinator etc.
    // log event about app launch time
    
    createAppInternalScreen()
  }
  
}
