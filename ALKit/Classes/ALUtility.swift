//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation

internal var printEnabled = true
internal var buildConfiguration: ALBuildConfig = .debug

// MARK: - ALReleaseTester
protocol ALReleaseTester {
  func testReleaseConfiguration()
}

// MARK: - ALService
protocol ALService { }

extension ALService {
  var name: String {
    return String(describing: type(of: self))
  }
  
  func aPrint(_ items: Any...) {
    guard printEnabled else { return }
    for item in items {
      let logRequiredPrefix = "*** "
      print(logRequiredPrefix + name + " -> \(item)\n")
    }
  }
}

// MARK: - ALConfiguredTrackable
protocol ALConfiguredTrackable {
  var isConfigured: Bool { get }
}

// MARK: - ALBuildConfig
public enum ALBuildConfig {
  case debug, adhoc, release
}
