//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation
import FirebaseCore
import FirebaseRemoteConfig

// MARK: - ALRemoteConfig
public protocol ALRemoteConfig {
  var remoteConfig: RemoteConfig { get }
  
  func screenType(forSpot spot: String) -> String?
  func value<T>(forKey key: String, ofType type: T.Type) -> T?
}

// MARK: - ALRemoteConfigDelegate
public protocol ALRemoteConfigDelegate: class {
  func remoteConfig(_ alRemoteConfig: ALRemoteConfig, didConfigureWithStatus status: RemoteConfigFetchStatus, error: Error?)
}

// MARK: - ALRemoteConfigImp
final class ALRemoteConfigImp: ALRemoteConfig, ALConfiguredTrackable, ALService {

  /*
   MARK: - Internal
   */
  
  var remoteConfig: RemoteConfig = {
    let config = RemoteConfig.remoteConfig()
    config.setDefaults(fromPlist: "RemoteConfig")
    return config
  }()
  
  var isConfigured: Bool = false
  
  var didFinishConfiguring: Closure?
  
  init(delegate: ALRemoteConfigDelegate, settings: ALSettings, handler: Closure?) {
    self.settings = settings
    self.delegate = delegate
    self.didFinishConfiguring = handler
    
    fetchSettings()
  }
  
  func value<T>(forKey key: String, ofType type: T.Type) -> T? {
    guard isConfigured else { return nil }
//    assert(isConfigured, "You can't get the value before the activation of the Remote Config!")
    
    // FIXME: Can't work with simple types in root config (Int, Double, Bool).
    // Exception: String
    func getValue(for configuration: JSON) -> T? {
      var finalValue: AnyHashable?
      var finalConfiguration = configuration
      let pathComponents = key.components(separatedBy: ".")
      for pathComponent in pathComponents {
        finalValue = finalConfiguration[pathComponent]
        if let value = finalValue as? JSON {
          finalConfiguration = value
        } else if let remoteConfigValue = finalValue as? RemoteConfigValue,
          let value = remoteConfigValue.stringValue,
          value.isEmpty == false
        {
          if let data = value.data(using: .utf8),
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
            let dictionary = jsonObject as? JSON
          {
            finalConfiguration = dictionary
          } else {
            finalValue = value
          }
        }
      }

      if let remoteConfigValue = finalValue as? RemoteConfigValue {
        finalValue = remoteConfigValue.stringValue
      }
      
      if let stringValue = finalValue as? String,
        let data = stringValue.data(using: .utf8),
        let jsonObject = try? JSONSerialization.jsonObject(with: data, options: .allowFragments),
        let dictionary = jsonObject as? JSON
      {
        finalValue = dictionary
      }

      return finalValue as? T
    }
    
    var keys = remoteConfig.keys(withPrefix: nil)
    if keys.count == 0 {
      keys = Set(remoteConfig.allKeys(from: .default))
    }

    var valuesConfiguration: JSON = [:]
    keys.forEach { (key) in
      valuesConfiguration[key] = remoteConfig[key]
    }

    return getValue(for: valuesConfiguration)
  }
  
  func screenType(forSpot spot: String) -> String? {
    guard
      let spotConfig = spotConfig(forSpot: spot),
      let screenTypeString = value(forKey: "\(spotConfig).screen_type", ofType: String.self)
      else { return nil }
    
    return screenTypeString
  }
  
  /*
   MARK: - Private
   */
  
  private func fetchSettings() {
    let duration: TimeInterval = buildConfiguration == .release ? 60*60 : 0 // 1h is enough for release
    
    remoteConfig.fetch(withExpirationDuration: duration) { (status, fetchError) in
      // It was fetched with success even at the first start without internet
      guard fetchError == nil else {
        self.aPrint("fetch -> Fetch error \(fetchError!)")
        self.callDelegateInMain(withStatus: status, error: fetchError)
        return
      }
      
      switch status {
      
      case .success:
        
        self.remoteConfig.activate(completionHandler: { [weak self] (activateError) in
          guard let self = self else { return }
          // You can't get value without it in DEBUG, cause I set up an asserton
          
          if let activateError = activateError {
            /*
             - No internet, first start
             - No internet, but data was cached
             - We have an internet, but the expiration duration is not expired
            */
            self.aPrint("fetch -> We use cached config!")
            self.callDelegateInMain(withStatus: status, error: activateError)
          } else {
            self.aPrint("fetch -> New remote config is activated")
            self.callDelegateInMain(withStatus: status, error: activateError)
          }
        })
      
      case .failure, .noFetchYet, .throttled:
        self.callDelegateInMain(withStatus: status, error: fetchError)
        
      @unknown default:
        fatalError()
      }

    }
  }
  
  private unowned var delegate: ALRemoteConfigDelegate
  private var settings: ALSettings

  private var formattedMediaSource: String? {
    if let string = settings.mediaSource {
      return string.lowercased().replacingOccurrences(of: " ", with: "_")
    }
    return nil
  }
  
  private func callDelegateInMain(withStatus status: RemoteConfigFetchStatus, error: Error?) {
    aPrint(#function)
    self.isConfigured = true
    DispatchQueue.main.async {
      self.delegate.remoteConfig(self, didConfigureWithStatus: status, error: error)
      self.didFinishConfiguring?()
    }
  }
  
  private func setupMediaQAIfNeededForTest() {
    // QA
    if settings.mediaSource == nil,
       value(forKey: "media_qa", ofType: JSON.self) != nil
    {
      settings.mediaSource = "media_qa"
    }
  }
  
  private func spotConfig(forSpot spot: String) -> String? {
    setupMediaQAIfNeededForTest()
    
    // Mediasource
    if let formattedMediaSource = formattedMediaSource,
       let config = value(forKey: "\(formattedMediaSource).\(spot).config", ofType: String.self)
    {
      return config
    }
      
    // Organic
    else {
      guard let config = value(forKey: "\(spot).config", ofType: String.self)
        else { return nil }
      return config
    }
  }
  
}

