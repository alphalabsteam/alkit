//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation
import Amplitude_iOS

// MARK: - ALAmplitude
public protocol ALAmplitude {
  func logEvent(_ eventName: ALEventName, properties: [ALParameterName : Any], policy: ALTrackablePolicy, timed: Bool)
  func endTimedEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any])
  func setUserProperty(_ userProperty: ALUserProperty, withValue value: String, policy: ALTrackablePolicy)
}

// MARK: - ALAmplitudeImp
final class ALAmplitudeImp: ALAmplitude, ALService, ALTrackableByPolicy, ALTimedTrackable {
  
  /*
   MARK: - ALAmplitude
   */
  
  func logEvent(_ eventName: ALEventName, properties: [ALParameterName : Any], policy: ALTrackablePolicy, timed: Bool) {
    guard check(policy, key: eventName) else {
      aPrint("Event '\(eventName)' can be logged just \(policy.rawValue)!")
      return
    }
    
    let addedString = properties.isEmpty == false ? "with parameters: \(properties)" : "-EMPTY-"
    
    if timed {
      aPrint("\(debugString)Save timed event '\(eventName)', \(addedString)")
      timedEventsDatesMap[eventName] = Date()
    } else {
      aPrint("\(debugString)Log event '\(eventName)', \(addedString)")
      guard buildConfiguration != .debug else { return }
      amplitude.logEvent(eventName, withEventProperties: properties)
    }
  }
  
  func endTimedEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any]) {
    guard checkTimedEvent(eventName) else {
      aPrint("End Timed Event '\(eventName)' can't be logged without start!")
      return
    }
    
    var newParameters = parameters
    newParameters["time"] = getTimeIntervalMillisecForTimedEvent(eventName)
    aPrint("\(debugString)Log End timed event '\(eventName)', with properties: \(newParameters)")
    
    guard buildConfiguration != .debug else { return }
    amplitude.logEvent(eventName, withEventProperties: newParameters)
  }
  
  func setUserProperty(_ userProperty: ALUserProperty, withValue value: String, policy: ALTrackablePolicy) {
    guard check(policy, key: userProperty) else {
      aPrint("Property '\(userProperty)' can be set up just \(policy.rawValue)!")
      return
    }

    aPrint("\(debugString)Set user Property '\(userProperty)' with value: \(value)")
    guard buildConfiguration != .debug else { return }
    let ampIdentify = AMPIdentify()
    let nsString = NSString(string: value)
    ampIdentify.set(userProperty, value: nsString)
    amplitude.identify(ampIdentify)
  }
  
  /*
   MARK: - ALTimedTrackble
   */
    
  func checkTimedEvent(_ event: String) -> Bool {
    if let _ = timedEventsDatesMap[event] {
      return true
    }
    return false
  }
  
  /*
   MARK: - ALTrackableByPolicy
   */
  
  var analyticsName: String {
    return "amplitude"
  }
  
  var oncePerSessionTrackableEvents = Set<String>()
  
  func check(_ policy: ALTrackablePolicy, key: String) -> Bool {
    var justCreatedSet: Bool = false
    if onceAtAllTrackableEvents == nil {
      let set = Set<String>()
      justCreatedSet = true
      setOnceAtAllTrackableEvents(set)
    }
    
    switch policy {
    case .multiple:
      if justCreatedSet == false, var events = onceAtAllTrackableEvents, events.contains(key) {
        events.remove(key)
        setOnceAtAllTrackableEvents(events)
      }
      return true
    case .onceAtAll:
      if justCreatedSet == false, onceAtAllTrackableEvents!.contains(key) {
        return false
      }
      var set = onceAtAllTrackableEvents!
      set.insert(key)
      setOnceAtAllTrackableEvents(set)
      return true
    case .oncePerSession:
      if oncePerSessionTrackableEvents.contains(key) {
        return false
      }
      oncePerSessionTrackableEvents.insert(key)
      return true
    }
  }
  
  /*
   MARK: - Life cycle
   */
  
  init(apiKey: String) {
    self.apiKey = apiKey
    testReleaseConfiguration()
    amplitude.initializeApiKey(apiKey)
  }

  /*
   MARK: - Private
   */
  
  private func getTimeIntervalMillisecForTimedEvent(_ event: String) -> Int {
    if let date = timedEventsDatesMap[event] {
      let timeInterval = Int((date.timeIntervalSinceNow * 1000.0).rounded().magnitude)
      return timeInterval
    }
    assertionFailure("На данный момент мы должны уже были проверить это!")
    return -1
  }
  
  private var timedEventsDatesMap = [ALEventName : Date]()
  
  private var amplitude: Amplitude {
    guard let amp = Amplitude.instance() else {
      fatalError()
    }
    return amp
  }
  
  private var debugString: String {
    return buildConfiguration == .debug ? "DEBUG MODE: " : ""
  }
  
  private var apiKey: String
  
}

// MARK: - ALReleaseTester
extension ALAmplitudeImp: ALReleaseTester {
  
  func testReleaseConfiguration() {
    guard buildConfiguration != .debug else { return }
    
    guard apiKey.isEmpty == false else {
      fatalError("Api key can't be empty, if you use this service!")
    }
  }
  
}


