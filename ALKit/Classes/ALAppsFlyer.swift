//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation
import AppsFlyerLib

// MARK: - ALAppsFlyerConfig
protocol ALAppsFlyerConfig {
  var appsFlyerDevKey: String { get }
  var appleAppID: String { get }
}

// MARK: - ALAppsFlyerDelegate
public protocol ALAppsFlyerDelegate: class {
  func appsFlyer(_ alAppsFlyer: ALAppsFlyer, didReceiveConversionData conversionInfo: [AnyHashable : Any]?, error: Error?)
}

// MARK: - ALAppsFlyerEvent
public enum ALAppsFlyerEvent {
  case startTrial(contentId: String, revenue: Float, currency: String)
  case purchase(contentId: String, revenue: Float, currency: String)
  
  var name: String {
    switch self {
    case .startTrial: return AFEventStartTrial
    case .purchase: return AFEventPurchase
    }
  }
  
  var properties: [String: Any] {
    switch self {
    case .startTrial(let contentId, let revenue, let currency):
      return [AFEventParamContentId: contentId,
              AFEventParamRevenue: revenue,
              AFEventParamCurrency: currency]
    case .purchase(let contentId, let revenue, let currency):
      return [AFEventParamContentId: contentId,
              AFEventParamRevenue: revenue,
              AFEventParamCurrency: currency]
    }
  }
}

// MARK: - ALAppsFlyer

public protocol ALAppsFlyer {
  var appsFlyer: AppsFlyerTracker { get }
  var isFirstLaunch: Bool { get }
  
  func trackEvent(_ event: ALAppsFlyerEvent)
  func registerUninstall(deviceToken: Data)
}

// MARK: - ALAppsFlyerImp
final class ALAppsFlyerImp: NSObject, ALAppsFlyer, ALService, ALConfiguredTrackable {
  
  /*
   MARK: - Internal
   */
  
  var appsFlyer: AppsFlyerTracker
  
  var isConfigured: Bool = false
  
  var didFinishConfiguring: Closure?
  
  var isFirstLaunch: Bool = false
  
  init(delegate: ALAppsFlyerDelegate, config: ALAppsFlyerConfig, handler: Closure?) {
    self.delegate = delegate
    self.config = config
    didFinishConfiguring = handler
    appsFlyer = AppsFlyerTracker.shared()
    settings = ALServiceLocatorImp.shared.alSettings
    super.init()
    
    testReleaseConfiguration()
    appsFlyer.trackAppLaunch()
    appsFlyer.appsFlyerDevKey = config.appsFlyerDevKey
    appsFlyer.appleAppID = config.appleAppID
    appsFlyer.delegate = self
    appsFlyer.isDebug = buildConfiguration == .debug
  }
  
  func trackEvent(_ event: ALAppsFlyerEvent) {
    appsFlyer.trackEvent(event.name, withValues: event.properties)
  }
  
  func registerUninstall(deviceToken: Data) {
    appsFlyer.registerUninstall(deviceToken)
  }
  
  /*
   MARK: - Private
   */
  
  private unowned var delegate: ALAppsFlyerDelegate
  private var config: ALAppsFlyerConfig
  private var settings: ALSettings
  
  private func callDelegateInMain(_ appsFlyer: ALAppsFlyer, didReceiveConversionData conversionInfo: [AnyHashable : Any]?, error: Error?) {
    aPrint(#function)
    isConfigured = true
    DispatchQueue.main.async {
      self.delegate.appsFlyer(appsFlyer, didReceiveConversionData: conversionInfo, error: error)
      self.didFinishConfiguring?()
    }
  }
  
  private var simulatedMediaSource: String? {
    guard
      buildConfiguration != .release,
      let alRemoteConfig = ALServiceLocatorImp.shared.alRemoteConfig,
      let isMediaSourceSimulationEnabled = alRemoteConfig.value(forKey: "media_source_simulation.enabled", ofType: Bool.self),
      isMediaSourceSimulationEnabled,
      let simulatedMediaSource = alRemoteConfig.value(forKey: "media_source_simulation.media_source", ofType: String.self)
      else {
        return nil
    }
    return simulatedMediaSource
  }
  
}

// MARK: - AppsFlyerTrackerDelegate
extension ALAppsFlyerImp: AppsFlyerTrackerDelegate {
  
  func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]!) {
    aPrint("Received conversion data")
    
    guard settings.mediaSource == nil else {
        callDelegateInMain(self, didReceiveConversionData: conversionInfo, error: nil)
        return
    }
    
    self.isFirstLaunch = true
    
    if let simulatedMediaSource = simulatedMediaSource {
      let formattedSource = simulatedMediaSource.lowercased().replacingOccurrences(of: " ", with: "_")
      settings.mediaSource = formattedSource
    } else if let source = conversionInfo?["media_source"] as? String {
      let formattedSource = source.lowercased().replacingOccurrences(of: " ", with: "_")
      settings.mediaSource = formattedSource
    }
    
    callDelegateInMain(self, didReceiveConversionData: conversionInfo, error: nil)
  }
  
  func onConversionDataFail(_ error: Error!) {
    aPrint("Failed to recieved conversion data")
    callDelegateInMain(self, didReceiveConversionData: nil, error: error)
  }
  
}

// MARK: - ALReleaseTester
extension ALAppsFlyerImp: ALReleaseTester {
  
  func testReleaseConfiguration() {
    guard buildConfiguration != .debug else { return }
    if config.appleAppID.isEmpty { fatalError("Error: Empty appleAppID!") }
    if config.appsFlyerDevKey.isEmpty { fatalError("Error: Empty appsFlyerDevKey!") }
    if CharacterSet.decimalDigits.isSuperset(of: CharacterSet(charactersIn: config.appleAppID)) == false {
      fatalError("Error: appleAppID should be a number! It contains other symbols")
    }
  }
  
}
