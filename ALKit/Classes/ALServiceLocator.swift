//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation

// MARK: - ALServiceLocator
public protocol ALServiceLocator {
  var alSettings: ALSettings! { get set }
  var alConfigurator: ALConfigurator { get }
  var alAppsFlyer: ALAppsFlyer! { get set }
  var alRemoteConfig: ALRemoteConfig! { get set }
  var alAmplitude: ALAmplitude! { get set }
  var alFlurry: ALFlurry! { get set }
}

// MARK: - ALServiceLocatorImp
public final class ALServiceLocatorImp: ALServiceLocator {
  
  /*
   MARK: - Public
   */
  
  public static var shared: ALServiceLocator = ALServiceLocatorImp()
  
  public lazy var alConfigurator: ALConfigurator = ALConfiguratorImp()
  public var alAppsFlyer: ALAppsFlyer!
  public var alSettings: ALSettings!
  public var alRemoteConfig: ALRemoteConfig!
  public var alAmplitude: ALAmplitude!
  public var alFlurry: ALFlurry!
  
  /*
   MARK: - Private
   */
  
  private init() { }
  
}
