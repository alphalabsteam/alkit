//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation

typealias JSON = Dictionary<String, AnyHashable>
typealias Closure = () -> Void

typealias ALUserDefaultsKey = String
extension ALUserDefaultsKey {
  static let userAttributionMediaSource = "userAttributionMediaSource"
  static let onceAtAllTrackableEvents = "onceAtAllTrackableEvents"
  static let sessionNumber = "sessionNumber"
  static let appFirstLaunchDate = "appFirstLaunchDate"
}

protocol Storage {
  func save(_ value: Any, forKey key: ALUserDefaultsKey)
  func value<T>(forKey key: String, ofType type: T.Type) -> T?
}

extension UserDefaults: Storage {
  func save(_ value: Any, forKey key: ALUserDefaultsKey) {
    set(value, forKey: key)
  }
  func value<T>(forKey key: ALUserDefaultsKey, ofType type: T.Type) -> T? {
    return value(forKey: key) as? T
  }
}

protocol Storable {
  var storage: Storage { get }
}

public protocol ALSettings {
  var mediaSource: String? { get set }
  var sessionNumber: Int { get }
  var appFirstLaunchDate: Date? { get }
  var appCurrentLaunchDate: Date { get }
}

final class ALSettingsImp: ALSettings, Storable, ALService {
  
  /*
   MARK: - Internal
   */
  
  var mediaSource: String? {
    get { return storage.value(forKey: .userAttributionMediaSource, ofType: String.self) }
    set { storage.save(newValue as Any, forKey: .userAttributionMediaSource) }
  }
  
  var sessionNumber: Int {
    get { return storage.value(forKey: .sessionNumber, ofType: Int.self) ?? 0 }
    set { storage.save(newValue as Any, forKey: .sessionNumber) }
  }
  
  var appFirstLaunchDate: Date? {
    get { return storage.value(forKey: .appFirstLaunchDate, ofType: Date.self) }
    set {
      guard appFirstLaunchDate == nil else {
        fatalError("We can setup it just onceAtAll!")
      }
      storage.save(newValue as Any, forKey: .appFirstLaunchDate) }
  }
  
  var appCurrentLaunchDate: Date = Date()
  
  var storage: Storage
  
  init(storage: Storage) {
    self.storage = storage
    updateSession()
  }
  
  /*
   MARK: - Private
   */
  
  private func updateSession() {
    sessionNumber += 1
    aPrint("Session number \(sessionNumber)")
    
    if sessionNumber == 1 {
      appFirstLaunchDate = Date()
      aPrint("First session start date is \(String(describing: appFirstLaunchDate!))")
    }
  }

}
