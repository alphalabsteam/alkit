//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation

// MARK: - ALConfigurator
public protocol ALConfigurator {
  @discardableResult
  func configure(delegate: ALConfiguratorDelegate, buildConfig: ALBuildConfig, timeOutSec: TimeInterval, modulePrintEnabled: Bool) -> ALConfigurator
  
  @discardableResult
  func configureAppsFlyer(delegate: ALAppsFlyerDelegate, appsFlyerDevKey: String, appleAppID: String) -> ALConfigurator
  
  @discardableResult
  func configureRemoteConfig(delegate: ALRemoteConfigDelegate) -> ALConfigurator
  
  @discardableResult
  func configureAmplitude(withApiKey apiKey: String) -> ALConfigurator
  
  @discardableResult
  func configureFlurry(withApiKey apiKey: String) -> ALConfigurator
}

// MARK: - ALConfiguratorDelegate
public protocol ALConfiguratorDelegate: class {
  func didConfigureRequiredServices(byTimeout: Bool, secondsPassed: TimeInterval)
}

// MARK: - ALConfiguratorImp
final class ALConfiguratorImp: ALConfigurator, ALConfiguredTrackable {
    
  /*
   MARK: - Internal
   */
  
  init() {
    locator = ALServiceLocatorImp.shared
    let settings = ALSettingsImp(storage: UserDefaults.standard)
    locator.alSettings = settings
    notifier = ALRequiredServicesNotifierImp(settings: settings)
    notifier.requiredConfigurableServices.append(self)
  }
  
  var isConfigured: Bool = false
  
  func configure(delegate: ALConfiguratorDelegate, buildConfig: ALBuildConfig, timeOutSec: TimeInterval, modulePrintEnabled: Bool) -> ALConfigurator {
    notifier.setup(delegate: delegate, timeOut: timeOutSec)
    buildConfiguration = buildConfig
    printEnabled = modulePrintEnabled
    isConfigured = true
    
    return self
  }
  
  func configureAppsFlyer(delegate: ALAppsFlyerDelegate, appsFlyerDevKey: String, appleAppID: String) -> ALConfigurator {
    guard isConfigured else { fatalError("You forgot to call configure(...) method!") }
    
    struct AppsFlyerConfig: ALAppsFlyerConfig {
      var appsFlyerDevKey: String
      var appleAppID: String
    }
    let config = AppsFlyerConfig(appsFlyerDevKey: appsFlyerDevKey, appleAppID: appleAppID)
    let appsFlyer = ALAppsFlyerImp(delegate: delegate, config: config, handler: notifier.appsFlyerHandler)
    notifier.requiredConfigurableServices.append(appsFlyer)
    locator.alAppsFlyer = appsFlyer

    return self
  }
  
  func configureRemoteConfig(delegate: ALRemoteConfigDelegate) -> ALConfigurator {
    guard isConfigured else { fatalError("You forgot to call configure(...) method!") }
    let remoteConfig = ALRemoteConfigImp(delegate: delegate, settings: locator.alSettings, handler: notifier.remoteConfigHandler)
    notifier.requiredConfigurableServices.append(remoteConfig)
    locator.alRemoteConfig = remoteConfig
    
    return self
  }
  
  func configureAmplitude(withApiKey apiKey: String) -> ALConfigurator {
    guard isConfigured else { fatalError("You forgot to call configure(...) method!") }
    locator.alAmplitude = ALAmplitudeImp(apiKey: apiKey)
    
    return self
  }
  
  func configureFlurry(withApiKey apiKey: String) -> ALConfigurator {
    guard isConfigured else { fatalError("You forgot to call configure(...) method!") }
    locator.alFlurry = ALFlurryImp(apiKey: apiKey)
    
    return self
  }
  
  /*
   MARK: - Private
   */
  
  private var notifier: ALRequiredServicesNotifier
  
  private var locator: ALServiceLocator
  
}

// MARK: - ALRequiredServicesNotifier
protocol ALRequiredServicesNotifier {
  var requiredConfigurableServices: [ALConfiguredTrackable] { get set }
  var remoteConfigHandler: Closure? { get }
  var appsFlyerHandler: Closure? { get }
  
  func setup(delegate: ALConfiguratorDelegate, timeOut: TimeInterval)
}

// MARK: - ALRequiredServicesNotifierImp
final class ALRequiredServicesNotifierImp: ALRequiredServicesNotifier, ALService {
  
  /*
   MARK: - Internal
   */
  
  init(settings: ALSettings) {
    self.settings = settings
  }
  
  var requiredConfigurableServices: [ALConfiguredTrackable] = []
  var remoteConfigHandler: Closure?
  var appsFlyerHandler: Closure?
  
  func setup(delegate: ALConfiguratorDelegate, timeOut: TimeInterval) {
    self.delegate = delegate
    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
      if self.weHaveOnlyGeneralRequiredService {
        self.tryToNotify()
      }
    }
    DispatchQueue.main.asyncAfter(deadline: .now() + timeOut) {
      if self.servicesAreConfigured == false {
        self.aPrint("Call didFinishConfiguringRequiredServices by timeout")
        self.delegate.didConfigureRequiredServices(byTimeout: true, secondsPassed: timeOut)
        self.notifyDelegateWasCalledByTimeout = true
      } else {
        self.aPrint("Timeout \(timeOut) sec")
      }
    }
    
    remoteConfigHandler = { [weak self] in
      self?.tryToNotify()
    }
    
    appsFlyerHandler = { [weak self] in
      self?.tryToNotify()
    }
  }
  
  /*
   MARK: - Private
   */
  
  private var settings: ALSettings
  
  private unowned var delegate: ALConfiguratorDelegate!
  
  private var weHaveOnlyGeneralRequiredService: Bool {
    return requiredConfigurableServices.count == 1
  }
    
  private var servicesAreConfigured: Bool {
    let notConfiguredServices = requiredConfigurableServices.filter { $0.isConfigured == false }
    return notConfiguredServices.isEmpty
  }
  
  private var notifyDelegateWasCalledByTimeout: Bool = false
    
  private func tryToNotify() {
    if servicesAreConfigured, notifyDelegateWasCalledByTimeout == false {
      aPrint("Call didFinishConfiguringRequiredServices cause all services are configured")
      let passedTime = self.getPassedTime()
      aPrint("Time for launch: \(passedTime) sec")
      DispatchQueue.main.async {
        self.delegate.didConfigureRequiredServices(byTimeout: false, secondsPassed: passedTime)
      }
    }
  }
  
  private func getPassedTime() -> TimeInterval {
    let sessionStartTime = self.settings.appCurrentLaunchDate
    let passedTime = round( 100 * Date().timeIntervalSince(sessionStartTime)) / 100
    return passedTime
  }
  
}
