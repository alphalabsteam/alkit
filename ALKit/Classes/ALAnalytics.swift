//
//  Copyright © 2019 Alpha Labs. All rights reserved.
//

import Foundation

// MARK: - ALEventName
public typealias ALEventName = String

// MARK: - ALParameterName
public typealias ALParameterName = String
public extension ALParameterName {
  static let action = "action"
  static let source = "source"
  static let count = "count"
  static let gender = "gender"
  static let userId = "userId"
}

// MARK: - ALUserProperty
public typealias ALUserProperty = String

// MARK: - ALTrackablePolicy
public enum ALTrackablePolicy: String {
  case multiple
  case onceAtAll
  case oncePerSession
}

// MARK: - ALAnalytics
public protocol ALAnalytics {
  func logEvent(_ eventName: ALEventName, policy: ALTrackablePolicy)
  func logEvent(_ eventName: ALEventName, withParameters eventParameters: [ALParameterName : Any], policy: ALTrackablePolicy)
  func setUserProperty(_ userProperty: ALUserProperty, withValue value: String, policy: ALTrackablePolicy)
}

// MARK: - ALTimedTrackble
protocol ALTimedTrackable {
  func checkTimedEvent(_ event: String) -> Bool
}

// MARK: - ALTrackableByPolicy
protocol ALTrackableByPolicy {
  var analyticsName: String { get }
  var oncePerSessionTrackableEvents: Set<String> { get set }
  func check(_ policy: ALTrackablePolicy, key: String) -> Bool
}

extension ALTrackableByPolicy {
  
  /*
   MARK: - Internal
   */
  
  var onceAtAllTrackableEvents: Set<String>? {
    guard
      let data = storage.value(forKey: key, ofType: Data.self),
      let decodedSet = NSKeyedUnarchiver.unarchiveObject(with: data) as? Set<String>
      else { return nil }
    return decodedSet
  }
  
  func setOnceAtAllTrackableEvents(_ events: Set<String>) {
    let encodedData = NSKeyedArchiver.archivedData(withRootObject: events)
    storage.save(encodedData, forKey: key)
  }
  
  /*
   MARK: - Private
   */
  
  private var key: String {
    return "\(analyticsName)_\(ALUserDefaultsKey.onceAtAllTrackableEvents)"
  }
  
  private var storage: Storage {
    return UserDefaults.standard
  }
  
}
