//
//  ALFlurry.swift
//  ALKit
//
//  Created by Nikita Didenko on 10/23/19.
//

import Foundation
import Flurry_iOS_SDK

// MARK: - ALFlurry
public protocol ALFlurry {
  func logEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any], policy: ALTrackablePolicy, timed: Bool)
  func endTimedEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any])
  func setUserProperty(_ userProperty: ALUserProperty, withValue value: String, policy: ALTrackablePolicy)
}

// MARK: - ALFlurryImp
final class ALFlurryImp: ALFlurry, ALService, ALTrackableByPolicy, ALTimedTrackable {
  
  /*
   MARK: - ALFlurry
   */
  
  func logEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any], policy: ALTrackablePolicy, timed: Bool) {
    guard check(policy, key: eventName) else {
      aPrint("Event '\(eventName)' can be logged just \(policy.rawValue)!")
      return
    }
    
    if timed { startTimedEvents.insert(eventName) }
    
    let addedString = parameters.isEmpty == false ? "with parameters: \(parameters)" : "-EMPTY-"
    aPrint("\(debugString)Log event '\(eventName)', \(addedString)")
    guard buildConfiguration != .debug else { return }
    
    Flurry.logEvent(eventName, withParameters: parameters, timed: timed)
  }
  
  func endTimedEvent(_ eventName: ALEventName, parameters: [ALParameterName : Any]) {
    guard checkTimedEvent(eventName) else {
      aPrint("EndTimedEvent '\(eventName)' can't be logged without start!")
      return
    }
    
    aPrint("\(debugString)Log End time event '\(eventName)', with properties: \(parameters)")
    guard buildConfiguration != .debug else { return }
    
    Flurry.endTimedEvent(eventName, withParameters: parameters)
  }
  
  func setUserProperty(_ userProperty: ALUserProperty, withValue value: String, policy: ALTrackablePolicy) {
    guard check(policy, key: userProperty) else {
      aPrint("Property '\(userProperty)' can be set up just onceAtAll!")
      return
    }

    aPrint("\(debugString)Set user Property '\(userProperty)' with value: \(value)")
    guard buildConfiguration != .debug else { return }
    
    switch userProperty {
    case .gender: Flurry.setGender(value)
    case .userId: Flurry.setUserID(value)
    default:
      aPrint("No such user property in Flurry")
    }
  }
  
  /*
   MARK: - ALTimedTrackble
   */
    
  func checkTimedEvent(_ event: String) -> Bool {
    if startTimedEvents.contains(event) {
      startTimedEvents.remove(event)
      return true
    }
    return false
  }
  
  /*
   MARK: - ALTrackableByPolicy
   */
  
  var analyticsName: String {
    return "flurry"
  }
  
  var oncePerSessionTrackableEvents = Set<String>()
  
  func check(_ policy: ALTrackablePolicy, key: String) -> Bool {
    var justCreatedSet: Bool = false
    if onceAtAllTrackableEvents == nil {
      let set = Set<String>()
      justCreatedSet = true
      setOnceAtAllTrackableEvents(set)
    }
    
    switch policy {
    case .multiple:
      if justCreatedSet == false, var events = onceAtAllTrackableEvents, events.contains(key) {
        events.remove(key)
        setOnceAtAllTrackableEvents(events)
      }
      return true
    case .onceAtAll:
      if justCreatedSet == false, onceAtAllTrackableEvents!.contains(key) {
        return false
      }
      var set = onceAtAllTrackableEvents!
      set.insert(key)
      setOnceAtAllTrackableEvents(set)
      return true
    case .oncePerSession:
      if oncePerSessionTrackableEvents.contains(key) {
        return false
      }
      oncePerSessionTrackableEvents.insert(key)
      return true
    }
  }
  
  /*
   MARK: - Life cycle
   */
  
  init(apiKey: String) {
    self.apiKey = apiKey
    testReleaseConfiguration()
    
    guard let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String else {
      assertionFailure("We need it, maaan..")
      return
    }

    let builder = FlurrySessionBuilder.init()
      .withAppVersion(appVersion)
      .withLogLevel(FlurryLogLevelAll)
      .withCrashReporting(true)
      .withSessionContinueSeconds(10)

    Flurry.startSession(apiKey, with: builder)
  }

  /*
   MARK: - Private
   */
  
  private var startTimedEvents = Set<String>()
  
  private var debugString: String {
    return buildConfiguration == .debug ? "DEBUG MODE: " : ""
  }
  
  private var apiKey: String
  
}

// MARK: - ALReleaseTester
extension ALFlurryImp: ALReleaseTester {
  
  func testReleaseConfiguration() {
    guard buildConfiguration != .debug else { return }
    
    guard apiKey.isEmpty == false else {
      fatalError("Api key can't be empty, if you use this service!")
    }
  }
  
}
