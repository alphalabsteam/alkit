#
#  Copyright © 2019 Alpha Labs. All rights reserved.
#

Pod::Spec.new do |s|
  s.name         = "ALKit"
  s.version      = "1.1.5"
  s.swift_version = "5.0"
  s.summary      = "Summary"
  s.description  = "Description"
  s.homepage     = "https://github.com/nsdidenko/ALKit"
  s.license      = "MIT"
  s.author       = { "Nikita Didenko" => "ns.didenko@mail.ru" }
  s.platform     = :ios, "11.0"
  s.source       = { :git => "https://github.com/nsdidenko/ALKit.git", :tag => "#{s.version}" }
  s.source_files  = "ALKit/Classes/**/*"
  s.static_framework = true
  s.vendored_frameworks = 'ALKit/Frameworks/*.framework'

  s.resource_bundles = {
    'ALKit' => [ 'ALKit/**/*.xcassets', 'ALKit/**/*.xib' ]
  }

  s.dependency 'AppsFlyerFramework'
  s.dependency 'Firebase/Core'
  s.dependency 'Firebase/RemoteConfig'
  s.dependency 'Amplitude-iOS'
  s.dependency 'Flurry-iOS-SDK/FlurrySDK'

#s.dependency 'Fabric'
#s.dependency 'Crashlytics'
#s.dependency 'Flurry-iOS-SDK/FlurryAds'
#s.dependency 'MBProgressHUD'
#s.dependency 'Alamofire'
#s.dependency 'DeviceKit'
#s.dependency 'FacebookCore'
#s.dependency 'FacebookLogin'
#  s.dependency 'Firebase/Analytics'
#  s.dependency 'Firebase/Auth'
#  s.dependency 'Firebase/Firestore'
#s.dependency 'AWSRekognition'

end
